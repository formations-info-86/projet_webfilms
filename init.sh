if [ ! -d "venv" ]; then
    echo --------------------
    echo Creating virtualenv
    echo --------------------
    python3 -m venv venv
fi
source venv/bin/activate

pip3 install -r requirements.txt

# export FLASK_APP=app.py
# flask run
