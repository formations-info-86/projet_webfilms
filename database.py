import sqlite3

DBNAME = "films.db"

def _select(requete, params=None):
    """ Exécute une requête type select"""
    with sqlite3.connect(DBNAME) as db:
        c = db.cursor()
        if params is None:
            c.execute(requete)
        else:
            c.execute(requete, params)
        res = c.fetchall()
    return res

def _requete(requete, params=None):
    """ Execute une requête type update/insert """
    with sqlite3.connect(DBNAME) as db:
        c = db.cursor()
        if params is None:
            c.execute(requete)
        else:
            c.execute(requete, params)
        db.commit()
    return

def get_all_directors():
    requete = """select distinct personne.id, personne.nom
                        from film, personne 
                        where personne.id=film.idRealisateur order by personne.nom"""
    return _select(requete)

def get_films_by(id_director):
    requete = """select film.titre, film.annee
                        from film
                        where film.idRealisateur=?
                        order by film.annee desc"""
    return _select(requete, params=(id_director,))
