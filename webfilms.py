from flask import render_template, request, Flask
import database as db

app = Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/realisateur_choix')
def realisateur_choix():
    real = db.get_all_directors()
    return render_template("realisateur_choix.html", realisateurs=real)

@app.route('/acteur_choix')
def acteur_choix():
    return render_template("acteur_choix.html")

@app.route('/credits')
def credits_():
    return render_template("credits.html")

@app.route('/films_de', methods=["POST"])
def films_de():
    id_real = request.values["id_real"]
    films = db.get_films_by(id_real)
    return render_template("liste_films.html", films=films)

if __name__ == "__main__":
    app.run()
